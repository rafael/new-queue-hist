# Waiting time histogram of the Debian NEW queue 

The `new-queue-hist.py` script produces a histograma of the waiting time of
packages in the Debian [NEW queue](https://ftp-master.debian.org/new.html).
Python and R are needed for running the script.

Example output:

![hist](new-queue-2018-01-21-10-27-22.png)
