#!/usr/bin/python

import urllib2
import re
import tempfile
import os
import sys
from html.parser import HTMLParser
from datetime import datetime, timedelta

prog = os.path.basename (sys.argv [0])

def barf (typ, msg):
    print ('%s:%s: %s' % (prog, typ, msg))
    if typ == 'E':
        sys.exit (1)

def cleanup (png = False):
    os.remove (fid_r.name)
    os.remove (fid_data.name)
    if png:
        os.remove (png_file)

url = 'https://ftp-master.debian.org/new.html'

curtime = datetime.now ()
date_tag = '%4d-%02d-%02d-%02d-%02d-%02d' % (curtime.year, curtime.month,
                                             curtime.day, curtime.hour,
                                             curtime.minute, curtime.second)
png_file = 'new-queue-%s.png' % date_tag

fid_data = tempfile.NamedTemporaryFile ('w', delete = False)

class MyHTMLParser (HTMLParser):
    def __init__ (self):
        HTMLParser.__init__ (self)
        self.deltas = []
    def handle_starttag(self, tag, attrs):
        if tag == "abbr":
            date = datetime.strptime (attrs [0] [1], '%a, %d %b %Y %H:%M:%S UTC')
            dt = curtime - date
            self.deltas.append (dt.days + dt.seconds / 24. / 60 / 60)

parser = MyHTMLParser ()
parser.feed (urllib2.urlopen (url).read ())

for dt in parser.deltas:
    fid_data.write ('%f\n' % dt)
fid_data.close ()

fid_r = tempfile.NamedTemporaryFile ('w', delete = False)
fid_r.write ('''x <- read.table ("%s")
png (file = "%s")
hist (x$V1, breaks = 40, xlab = "days in the NEW queue", col = "pink",
      ylab = "number of packages", las = 1, main = "%s")
dummy <- dev.off ()
''' % (fid_data.name, png_file, date_tag))
fid_r.close ()

if os.system ('Rscript %s' % fid_r.name) != 0:
    cleanup (png = True)
    barf ('E', 'Could not run R script')
else:
    barf ('I', 'Output PNG file: %s' % png_file)
    cleanup ()
